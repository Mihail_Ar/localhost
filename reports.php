<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Нет, не слипнется</title>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<!-- bootstrap -->
	<link rel="shortcut icon" href="img/ico.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<?php require_once('templates/header.php'); ?>

	<section id="reports">
		<div class="container rectangle">
			<table class="table">
				<thead>
					<tr>
						<th>№ </th>
						<th>Отчёт </th>
						<th>Кнопка </th>
					</tr>
				</thead>
				<tbody>

					<tr>
						<td>
							1.
						</td>
						<td>
							<form action="php_scripts/report.php" method="get">
								<input type="hidden" name="id" value="1">
								Посмотреть <input type="number" name="param"> покупателей с максимальной скидкой.								
						</td>
						<td>
							<button type="submit">Посмотреть </button>
							</form>							
						</td>
					</tr>

					<tr>
						<td>
							2.
						</td>
						<td>
							<form action="php_scripts/report.php" method="get">
								<input type="hidden" name="id" value="2">
								Посмотреть торты с содержанием <input type="text" name="param" placeholder="Название продукта в И.П.">								
						</td>
						<td>
							<button type="submit">Посмотреть </button>
							</form>							
						</td>
					</tr>
					
					<tr>
						<td>
							3.
						</td>
						<td>
							<form action="php_scripts/report.php" method="get">
								<input type="hidden" name="id" value="3">
								Посмотреть <input type="number" name="param"> менее калорийных тортов
						</td>
						<td>
							<button type="submit">Посмотреть </button>
							</form>							
						</td>
					</tr>
					
					<tr>
						<td>
							4.
						</td>
						<td>
							<form action="php_scripts/report.php" method="get">
								<input type="hidden" name="id" value="4">
								Посмотреть <input type="number" name="param"> самых тяжелых тортов
						</td>
						<td>
							<button type="submit">Посмотреть </button>
							</form>							
						</td>
					</tr>
					
				</tbody>
			</table>
		</div>

	</section>

	<?php require_once('templates/footer.php'); ?>

</body>
</html>