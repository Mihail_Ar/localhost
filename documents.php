<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Нет, не слипнется</title>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<!-- bootstrap -->
	<link rel="shortcut icon" href="img/ico.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<?php require_once('templates/header.php'); ?>

	<section id="journal">
		
		<?php require_once("templates/menu-link.php") ?>
		
		<div class="container docName">
			Журнал учета отпуска и поступления товара
		</div>
		<?php 
			if(isset($_COOKIE['user'])):
		?>
		<div class="container">
			<table class="table table-dark">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Наименование</th>
						<th scope="col">Количество (шт)</th>
					</tr>
				</thead>
				
				<?php require_once("templates/doc_table.php"); ?>
				
			</table>
		</div>
		<?php 
			else:
		?>
		<img src="img/unknown.jpg" alt="Неизвестный пользователь">
		<p><a href="login.php">Войдите в аккаунт, чтобы увидеть данные.</a></p>
		<?php 
			endif;
		?>
	</section>
	
	<?php require_once('templates/footer.php'); ?>

</body>
</html>