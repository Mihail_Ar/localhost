<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Нет, не слипнется</title>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<!-- bootstrap -->
	<link rel="shortcut icon" href="img/ico.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<?php 
		$from='office';
		require_once('templates/header.php');
	?>

	<section id="office">
		
		<?php require_once("templates/menu-link.php") ?>
		

		<div class="container-fluid hello">
			<div class="row">
				<div class="col-md-4">
					<img src="img/man_left.png" alt="Пряня">
				</div>
				<div class="col-md-4">
					
					<?php 
						if($_COOKIE['user'] == ''): 
					?>

					<p style="padding-top: 100px;">ПРИВЕТ, АМЛЕТ!</p>
					<?php else: ?>
					<p style="padding-top: 100px;">
						<?php 
							echo "ПРИВЕТ, {$_COOKIE['user']}"; 
						?>
					</p>
					<?php endif; ?>
				</div>
				<div class="col-md-4">
					<img src="img/man_right.png" alt="Пряня">
				</div>				
			</div>
		</div>

		<div>
			<a href="crud.php" style=" font-family: Heebo; font-style: normal; font-weight: normal; font-size: 40px; text-decoration-line: underline; color: rgba(0, 0, 0, 0.6);">Посмотреть информацию о товарах</a>
		</div>

		<div class="container rectangle">
			<h1>Где мы находимся: </h1>
			<div class="row">					
				<div class="col-md-3"></div>
				<div class="col-md-6">

					<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aab9f1671882bdcf787340517d7a066f7a4f8885ec1ffc0322039dc022b89cf7b&amp;width=468&amp;height=382&amp;lang=ru_RU&amp;scroll=true"></script>	

				</div>
				<div class="col-md-3"></div>			
			</div>
		</div>

		<div>
			<a href="reports.php" style=" font-family: Heebo; font-style: normal; font-weight: normal; font-size: 40px; text-decoration-line: underline; color: rgba(0, 0, 0, 0.6);">Посмотреть отчёты</a>			
		</div>

	</section>

	<?php require_once('templates/footer.php'); ?>

</body>
</html>