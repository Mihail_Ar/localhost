<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Нет, не слипнется</title>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<!-- bootstrap -->
	<link rel="shortcut icon" href="img/ico.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	
	<?php require_once("templates/header.php"); ?>

	<section id="crud">
		
		<?php require_once("templates/menu-link.php") ?>
		
		<div class="container rectangle" style="margin-top: 30px;">
			
			Поиск по записям
			<form action="php_scripts/search.php" method="get">
				<input type="text" name="query" placeholder="введите значение">
				<button type="submit">Найти</button>
			</form>
			<br>

			<a class="page-link" href="?inf=true">Показать дополнительную информацию</a>
			<a class="page-link" href="?inf=false">Скрыть дополнительную информацию</a>					

			<table class="table">
				
				<?php require_once("php_scripts/read_db.php") ?>

			</table>

			<table class="table" style="margin-top: 5px">
				<thead>
					<tr>
						<th>Добавьте новую запись:</th>
					</tr>
				</thead>
				<tbody>
					<form action="php_scripts/create_db.php" method="post">
						<tr>
							<th> <input placeholder="Название" type="text" name="name" required> </th>
							<th> <input placeholder="Цена" type="number" name="price" required> </th>
							<th> <input placeholder="Количество" type="number" name="quantity" required> </th>
						</tr>
						<tr>
							<th> <input placeholder="Ккал" type="number" name="calory" required> </th>
							<th> <input placeholder="Масса(гр.)" type="number" name="mass_gr" required> </th>
							<th> <input placeholder="Белки" type="number" name="proteins_gr" required> </th>
						</tr>
						<tr>
							<th> <input placeholder="Жиры" type="number" name="fats_gr" required> </th>
							<th> <input placeholder="Углеводы" type="number" name="carbs_gr" required> </th>

							<th> <button class="add-button" type="submit">ДОБАВИТЬ</button> </th>
						</tr>
						</form>
					
				</tbody>
			</table>

		</div>

	</section>

	<?php require_once("templates/footer.php"); ?>
	
</body>
</html>