<?php 
	
	require_once("connection_db.php");

	$query_id = $_GET['id'];
	switch ($query_id) 
	{
		case 1:
			$limit = $_GET['param'];
			$result = $mysql->query("SELECT * FROM `users` INNER JOIN `discount_cards` ON `discount_cards`.`user_id`=`users`.`id` ORDER BY `discount_cards`.`discount` DESC LIMIT $limit");
			$output = '<thead>';
				$output .= '<tr>';
					$output .= '<th>№ </th>';
					$output .= '<th>Покупатель </th>';
					$output .= '<th>Скидка </th>';
				$output .= '</tr>';
			$output .= '</thead>';
			$output .= '<tbody>';
			$nom = 0;

				while ($item = $result->fetch_assoc()) 
				{
					$output .= '<tr>';
						$output .= '<td>'.++$nom.'</td>';
						$output .= '<td>'.$item['name'].'</td>';
						$output .= '<td>'.$item['discount'].'</td>';
					$output .= '</tr>';
				}
			$output .= '</tbody>';
			
			break;

		case 2: 
			$text = $_GET['param'];
			$result = $mysql->query("SELECT `cakes_price`.`name` FROM `cakes_price` INNER JOIN `cakes_struct` ON `cakes_price`.`id`=`cakes_struct`.`cake_id` INNER JOIN `products` ON `cakes_struct`.`product_id` = `products`.`id` WHERE `products`.`name` = '$text'");
			$output = '<thead>';
				$output .= '<tr>';
					$output .= '<th>№ </th>';
					$output .= '<th>Торт </th>';
				$output .= '</tr>';
			$output .= '</thead>';
			$output .= '<tbody>';
			$nom = 0;

				while ($item = $result->fetch_assoc()) 
				{
					$output .= '<tr>';
						$output .= '<td>'.++$nom.'</td>';
						$output .= '<td>'.$item['name'].'</td>';
					$output .= '</tr>';
				}
			$output .= '</tbody>';
			break;
		
		case 3:
			$limit = $_GET['param'];
			$result = $mysql->query("SELECT * FROM `cakes_price` INNER JOIN `cakes_info` ON `cakes_price`.`id`=`cakes_info`.`cake_price_id` ORDER BY `cakes_info`.`calory` ASC LIMIT $limit");
			
			$output = '<thead>';
				$output .= '<tr>';
					$output .= '<th>№ </th>';
					$output .= '<th>Торт </th>';
					$output .= '<th>Калории </th>';
				$output .= '</tr>';
			$output .= '</thead>';
			$output .= '<tbody>';
			$nom = 0;

				while ($item = $result->fetch_assoc()) 
				{
					$output .= '<tr>';
						$output .= '<td>'.++$nom.'</td>';
						$output .= '<td>'.$item['name'].'</td>';
						$output .= '<td>'.$item['calory'].'</td>';
					$output .= '</tr>';
				}
			$output .= '</tbody>';
			break;
		
		case 4:
			$limit = $_GET['param'];
			$result = $mysql->query("SELECT * FROM `cakes_price` INNER JOIN `cakes_info` ON `cakes_price`.`id`=`cakes_info`.`cake_price_id` ORDER BY `cakes_info`.`mass_gr` DESC LIMIT $limit");
			
			$output = '<thead>';
				$output .= '<tr>';
					$output .= '<th>№ </th>';
					$output .= '<th>Торт </th>';
					$output .= '<th>Масса в граммах </th>';
				$output .= '</tr>';
			$output .= '</thead>';
			$output .= '<tbody>';
			$nom = 0;

				while ($item = $result->fetch_assoc()) 
				{
					$output .= '<tr>';
						$output .= '<td>'.++$nom.'</td>';
						$output .= '<td>'.$item['name'].'</td>';
						$output .= '<td>'.$item['mass_gr'].'</td>';
					$output .= '</tr>';
				}
			$output .= '</tbody>';
			break;
		
		default:
			$output = "Это что за запрос?";
			break;
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Нет, не слипнется</title>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<!-- bootstrap -->
	<link rel="shortcut icon" href="../img/ico.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>

	<?php require_once("../templates/header.php"); ?>

	<section>
		<div class="container rectangle">
			<table class="table">
				<?=$output;  ?>
			</table>
		<a href="../reports.php">Вернуться</a>
		</div>
	</section>

	<?php require_once("../templates/footer.php"); ?>

</body>
</html>