<?php 
	
	include_once("connection_db.php");

	$param = $mysql->real_escape_string($_GET['query']);		
	$result = $mysql->query("
		SELECT * FROM `cakes_price` INNER JOIN `cakes_info` 
		ON `cakes_price`.`id` = `cakes_info`.`cake_price_id`
		WHERE `cakes_price`.`name` = '$param'
		OR `cakes_price`.`price` = '$param'
		OR `cakes_price`.`quantity` = '$param'
		OR `cakes_info`.`calory` = '$param'
		OR `cakes_info`.`mass_gr` = '$param'
		OR `cakes_info`.`proteins_gr` = '$param'
		OR `cakes_info`.`fats_gr` = '$param'
		OR `cakes_info`.`carbs_gr` = '$param' 
		");
	


	$output = '<thead>';
	$output .= '<tr>';
	
	$output .= '<th>Название</th>';
	$output .= '<th>Цена</th>';
	$output .= '<th>Количество</th>';

	$output .= '<th>Ккал</th>';
	$output .= '<th>Вес(гр.)</th>';
	$output .= '<th>Белки</th>';
	$output .= '<th>Жиры</th>';
	$output .= '<th>Углеводы</th>';

	$output .= '<th>Изменить</th>';
	$output .= '<th>Удалить</th>';

	$output .= '</tr>';
	$output .= '</thead>';

	$output .= '<tbody>';

	while ($item = $result->fetch_assoc()) 
	{
		$output .= "<tr>";
		
		$output .= "<td>";
		$output .= htmlspecialchars($item['name']);
		$output .= "</td>";

		$output .= "<td>";
		$output .= htmlspecialchars($item['price']);
		$output .= "</td>";

		$output .= "<td>";
		$output .= htmlspecialchars($item['quantity']);
		$output .= "</td>";

		$output .= "<td>";
		$output .= htmlspecialchars($item['calory']);
		$output .= "</td>";

		$output .= "<td>";
		$output .= htmlspecialchars($item['mass_gr']);
		$output .= "</td>";

		$output .= "<td>";
		$output .= htmlspecialchars($item['proteins_gr']);
		$output .= "</td>";

		$output .= "<td>";
		$output .= htmlspecialchars($item['fats_gr']);
		$output .= "</td>";

		$output .= "<td>";
		$output .= htmlspecialchars($item['carbs_gr']);
		$output .= "</td>";
	

		$output .= "<td>";
		$output .= '<form action="../php_scripts/update_db.php" method="post">';
		$output .= '<input name="id" type="hidden" value="' . $item['id'] . '">';
		$output .= '<button class="update-button">ИЗМЕНИТЬ</button> </form>';
		$output .= "</td>";

		$output .= "<td>";
		$output .= '<form action="../php_scripts/delete_db.php" method="post">';
		$output .= '<input name="id" type="hidden" value="' . $item['id'] . '">';
		$output .= '<button class="delete-button">УДАЛИТЬ</button> </form>';
		$output .= "</td>";

		$output .= "</tr>";
	}

	$output .= '</tbody>';

	//echo $output;


?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Нет, не слипнется</title>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<!-- bootstrap -->
	<link rel="shortcut icon" href="../img/ico.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>

	<?php require_once("../templates/header.php"); ?>

	<section>
		<div class="container rectangle">
			<table class="table">
				<?=$output;  ?>
			</table>
		<a href="../crud.php">Вернуться</a>
		</div>
	</section>

	<?php require_once("../templates/footer.php"); ?>

</body>
</html>