<?php 

	include_once("connection_db.php");
	if (isset($_GET['query'])) 
	{
		$param = $mysql->real_escape_string($_GET['query']);
		$result = $mysql->query("SELECT * 
			FROM `cakes_price` INNER JOIN `cakes_info` 
			ON `cakes_price`.`id` = `cakes_info`.`cake_price_id`
			WHERE `cakes_price`.`name` == '$param' 
			OR `cakes_price`.`price` == '$param' 
			OR `cakes_price`.`quantity` == '$param' 
			OR `cakes_info`.`calory` == '$param'
			OR `cakes_info`.`mass_gr` == '$param'
			OR `cakes_info`.`proteins_gr` == '$param'
			OR `cakes_info`.`fats_gr` == '$param'
			OR `cakes_info`.`carbs_gr` == '$param'");
	}
	else
	{
		$result = $mysql->query("SELECT * FROM `cakes_price` INNER JOIN `cakes_info` ON `cakes_price`.`id` = `cakes_info`.`cake_price_id`");		
	}

	$flag;
	if(isset($_GET['inf']))
	{
		if(strcasecmp($_GET['inf'], "true") == 0)
			$flag = true;
		else
			$flag = false;
	}
	else
		$flag = false;

	$output = '<thead>';
	$output .= '<tr>';
	
	$output .= '<th>Название</th>';
	$output .= '<th>Цена</th>';
	$output .= '<th>Количество</th>';

	if ($flag) 
	{
		$output .= '<th>Ккал</th>';
		$output .= '<th>Вес(гр.)</th>';
		$output .= '<th>Белки</th>';
		$output .= '<th>Жиры</th>';
		$output .= '<th>Углеводы</th>';
	}

	$output .= '<th>Изменить</th>';
	$output .= '<th>Удалить</th>';

	$output .= '</tr>';
	$output .= '</thead>';

	$output .= '<tbody>';

	while ($item = $result->fetch_assoc()) 
	{
		$output .= "<tr>";
		
		$output .= "<td>";
		$output .= htmlspecialchars($item['name']);
		$output .= "</td>";

		$output .= "<td>";
		$output .= htmlspecialchars($item['price']);
		$output .= "</td>";

		$output .= "<td>";
		$output .= htmlspecialchars($item['quantity']);
		$output .= "</td>";

		if ($flag) 
		{
			$output .= "<td>";
			$output .= htmlspecialchars($item['calory']);
			$output .= "</td>";
	
			$output .= "<td>";
			$output .= htmlspecialchars($item['mass_gr']);
			$output .= "</td>";
	
			$output .= "<td>";
			$output .= htmlspecialchars($item['proteins_gr']);
			$output .= "</td>";
	
			$output .= "<td>";
			$output .= htmlspecialchars($item['fats_gr']);
			$output .= "</td>";
	
			$output .= "<td>";
			$output .= htmlspecialchars($item['carbs_gr']);
			$output .= "</td>";
		}

		$output .= "<td>";
		$output .= '<form action="../php_scripts/update_db.php" method="post">';
		$output .= '<input name="id" type="hidden" value="' . $item['id'] . '">';
		$output .= '<button class="update-button">ИЗМЕНИТЬ</button> </form>';
		$output .= "</td>";

		$output .= "<td>";
		$output .= '<form action="../php_scripts/delete_db.php" method="post">';
		$output .= '<input name="id" type="hidden" value="' . $item['id'] . '">';
		$output .= '<button class="delete-button">УДАЛИТЬ</button> </form>';
		$output .= "</td>";

		$output .= "</tr>";
	}

	$output .= '</tbody>';

	echo $output;

	$mysql->close();
?>