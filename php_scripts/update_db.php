<?php 

	include_once("connection_db.php");

	$id=$_POST['id'];

	$result = $mysql->query("SELECT * FROM `cakes_price` INNER JOIN `cakes_info` ON `cakes_price`.`id` = `cakes_info`.`cake_price_id` WHERE `cakes_price`.`id` = '$id'");
	$prev_line = $result->fetch_assoc();

	if ( isset($_POST['name']) ) //Если данная переменная не установлена, значит пользователь ещё не ввел данные, но уже перешел на страницу
	{
		$name = $mysql->real_escape_string($_POST['name']);
		$price = (int)$_POST['price'];
		$quantity = (int)$_POST['quantity'];

		$calory = (int)$_POST['calory'];
		$mass_gr = (int)$_POST['mass_gr'];
		$proteins_gr = (int)$_POST['proteins_gr'];
		$fats_gr = (int)$_POST['fats_gr'];
		$carbs_gr = (int)$_POST['carbs_gr'];

		$mysql->query("UPDATE `cakes_price` SET `name`='$name',`price`=$price,`quantity`=$quantity WHERE `id`=$id");
		$mysql->query("UPDATE `cakes_info` SET `calory`='$calory',`mass_gr`=$mass_gr,`proteins_gr`=$proteins_gr,`fats_gr`=$fats_gr,`carbs_gr`=$carbs_gr WHERE `cake_price_id`=$id");

		$mysql->close();

		header("Location: ../crud.php");
	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Нет, не слипнется</title>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<!-- bootstrap -->
	<link rel="shortcut icon" href="../img/ico.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>

	<?php require_once("../templates/header.php"); ?>

	<div class="container rectangle">
		<h1>Вы хотите изменить данную запись:</h1>
		<table class="table">
			<thead>
				<tr>
					<th><?="$prev_line[name]"?></th>
					<th><?="$prev_line[price]"?></th>
					<th><?="$prev_line[quantity]"?></th>
				</tr>
				<tr>
					<th><?="$prev_line[calory]"?></th>
					<th><?="$prev_line[mass_gr]"?></th>
					<th><?="$prev_line[proteins_gr]"?></th>
				</tr>
				<tr>
					<th><?="$prev_line[fats_gr]"?></th>
					<th><?="$prev_line[carbs_gr]"?></th>
				</tr>
			</thead>
			<tbody>
				<form action="<?=$_SERVER['PHP_SELF'];?>" method="post">
					<tr>
						<input type="hidden" name="id" value="<?=$id?>">
						<th><input type="text" name="name" value="<?=$prev_line['name']?>" required></th>
						<th><input type="number" name="price" value="<?=$prev_line['price']?>" required></th>
						<th><input type="number" name="quantity" value="<?=$prev_line['quantity']?>" required></th>
					</tr>
					<tr>
						<th><input type="number" name="calory" value="<?=$prev_line['calory']?>" required></th>
						<th><input type="number" name="mass_gr" value="<?=$prev_line['mass_gr']?>" required></th>
						<th><input type="number" name="proteins_gr" value="<?=$prev_line['proteins_gr']?>" required></th>
					</tr>
					<tr>
						<th><input type="number" name="fats_gr" value="<?=$prev_line['fats_gr']?>" required></th>
						<th><input type="number" name="carbs_gr" value="<?=$prev_line['carbs_gr']?>" required></th>
						<th><button type="submit" class="update-button">ИЗМЕНИТЬ</button></th>
					</tr>
				</form>
			</tbody>
		</table>
	</div>

	<?php require_once("../templates/footer.php"); ?>

</body>
</html>