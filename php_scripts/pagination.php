<?php 
	
	$mysql = new mysqli('localhost','root','','web_lab');
	$SQLresult = $mysql->query("SELECT COUNT(*) as count FROM `cakes`");
	$result = $SQLresult->fetch_assoc();
	$count = $result['count'];

	$page = isset($_GET["page"]) ? $_GET["page"] : 1;
	$line_limit = 2;
	$last_page_num = ceil($count/(2*$line_limit));
	$prev = ($page-1)>0 ? $page-1 : $page;
	$next = ($page+1)<=$last_page_num ? $page+1 : $page;

	$output ='<nav>';
	$output.='<ul class="pagination pagination-lg">';
	
	$output.='<li class="page-item">';
	$output.='<a class="page-link" href="?page='.$prev.'"><=</a>';
	$output.='</li>';

	for ($i=1; $i <=$last_page_num ; $i++) 
	{ 
		$output.='<li class="page-item">';
		$output.='<a class="page-link" href="?page='.$i.'">'.$i.'</a>';
		$output.='</li>';
	}

	$output.='<li class="page-item">';
	$output.='<a class="page-link" href="?page='.$next.'">=></a>';
	$output.='</li>';

	$output.='</ul>';
	$output.='</nav>';
	echo $output;
	$mysql->close();

?>
