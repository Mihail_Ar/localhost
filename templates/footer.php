<section id="footer">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
				<h3 class="ending">Контакты:</h3>
				<div class="row">
					<div class="col-md-2">
						<img src="img/geo_point.png" alt="geoposition">
					</div>
					<div class="col-md-10 ending head-end">
						Адрес:
					</div>
				</div>
				<div class="ending left_End">
					г. Волгоград, пр-т Ленина, 
					<br>д.73 (ост. “пл. Возрождения”) 
					<br>тел.: +89037654321
				</div>
			</div>
			<div class="col-md-6">
				<h3 class="ending">Режим работы:</h3>					 
				<div class="ending right_End">
					<br>Понедельник - пятница: 8:30-19:00 
					<br>Суббота: 9:30-18:00 
					<br>Воскресение: 9:30-16:00	
					<div style="text-align: right;">
						<a href="documents.php" style="color: #BABABA;">Журнал учета отпуска и <br>поступления товара</a>			
					</div>						
				</div>			
			</div>				
		</div>
	</div>
</section>