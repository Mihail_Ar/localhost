<?php
	function line_draw($left, $right)
	{
		$output='';
		if($left>0)
		{
			$output.='<div class="col-md-6">';//отрисовываем левую часть строки
				$output.='<div class="row">';
					$output.='<div class="col-md-8">';
						$output.='<img src="'.$left[2].'" alt="Торт">';
					$output.='</div>';
					$output.='<div class="col-md-4 name">';
						$output.=$left[1];
					$output.='</div>';
				$output.='</div>';
			$output.='</div>';//закончили отрисовку левой части строки
					
			$output.='<div class="col-md-6">';//отрисовываем правую часть строки
				$output.='<div class="row">';
					$output.='<div class="col-md-8">';
						$output.='<img src="'.$right[2].'" alt="Торт">';
					$output.='</div>';
					$output.='<div class="col-md-4 name">';
						$output.=$right[1];
					$output.='</div>';
				$output.='</div>';
			$output.='</div>';//закончили отрисовку правой части строки
		}
		return $output;
	}

	$mysql = new mysqli('localhost','root','','web_lab');
	$output ='';

	$LIMIT=$line_limit*2;
	$OFFSET=$LIMIT*($page-1);
	$result = $mysql->query("SELECT * FROM `cakes` LIMIT $LIMIT OFFSET $OFFSET");
	$items=$result->fetch_all();
	$kol=count($items)/2;

	for ($i=0; $i < $kol; $i++) 
	{ 
		$output.='<div class="container line">';//отрисовываем строку
			$output.='<div class="row">';
			{
				$output.=line_draw($items[2*$i],$items[2*$i+1]);
			}
			$output.='</div>';
		$output.='</div>';//закончили отрисовку линии
	}

	echo $output;
	$mysql->close();	
?>
