<?php 
	
	include_once("php_scripts/connection_db.php");
	
	$result=$mysql->query("SELECT * FROM `cakes_price`");

	$output = '<tbody>';
	$i=1;

	while ($item = $result->fetch_assoc()) 
	{
		$output .= '<tr>';
		$output .= '<th scope="row">' . $i . '</th>';
		$output .= '<td>' . $item['name'] . '</td>';
		$output .= '<td>' . $item['quantity'] . '</td>';
		$output .= '</tr>';
		$i++;
	}

	$output .='</tbody>';

	echo $output;

?>