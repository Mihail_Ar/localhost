-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Дек 29 2020 г., 19:33
-- Версия сервера: 10.4.14-MariaDB
-- Версия PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `web_lab`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cakes`
--

CREATE TABLE `cakes` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(500) CHARACTER SET utf8 NOT NULL,
  `image_url` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `cakes`
--

INSERT INTO `cakes` (`id`, `name`, `image_url`) VALUES
(1, 'Торт “Киевский”', '../img/cake1.png'),
(2, 'Торт <br>“Три шоколада”', '../img/cake2.png'),
(3, 'Торт “Радуга”', '../img/cake3.png'),
(4, 'Торт “Наполеон”', '../img/cake4.png'),
(5, 'Торт<br>“Красный бархат”', '../img/cake5.png'),
(6, 'Торт “Прага”', '../img/cake6.png'),
(7, 'Торт “Муссовый”<br><p class=\"note\">*вкусы уточняйте <br>на кассе</p>', '../img/cake7.png'),
(8, 'Торт “Медовик”', '../img/cake8.png'),
(9, 'Торт “Морковка”', '../img/cake9.png'),
(10, 'Торт<br>“Анна Павлова”', '../img/cake10.png'),
(11, 'Пирожное “Капкейки”<br><p class=\"note\">*вкусы уточняйте <br>на кассе</p>', '../img/cake11.png'),
(12, 'Пирожное “Профитроли”<br><p class=\"note\">*вкусы уточняйте<br>на кассе</p>', '../img/cake12.png'),
(13, 'Пирожное “Корзинка”<br><p class=\"note\">*вкусы уточняйте <br>на кассе</p>', '../img/cake13.png'),
(14, 'Пирожное “Буше”<br><p class=\"note\">*вкусы уточняйте <br>на кассе</p>', '../img/cake14.png'),
(15, 'Пирожное “Сицилий-<br>ские трубочки”<br><p class=\"note\">*вкусы уточняйте <br>на кассе</p>', '../img/cake15.png'),
(16, 'Пирожное “Шу”', '../img/cake16.png'),
(17, 'Молочный шоколад', '../img/cake17.png'),
(18, 'Горький шоколад', '../img/cake18.png');

-- --------------------------------------------------------

--
-- Структура таблицы `cakes_info`
--

CREATE TABLE `cakes_info` (
  `id` int(11) UNSIGNED NOT NULL,
  `cake_price_id` int(11) UNSIGNED NOT NULL,
  `calory` int(10) UNSIGNED NOT NULL,
  `mass_gr` int(10) UNSIGNED NOT NULL,
  `proteins_gr` int(10) UNSIGNED NOT NULL,
  `fats_gr` int(10) UNSIGNED NOT NULL,
  `carbs_gr` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Дамп данных таблицы `cakes_info`
--

INSERT INTO `cakes_info` (`id`, `cake_price_id`, `calory`, `mass_gr`, `proteins_gr`, `fats_gr`, `carbs_gr`) VALUES
(1, 1, 400, 300, 100, 100, 100),
(2, 2, 450, 3000, 1250, 1000, 750),
(3, 3, 4000, 350, 150, 110, 90),
(4, 4, 480, 500, 100, 150, 150),
(5, 5, 730, 600, 200, 300, 100),
(6, 6, 500, 480, 80, 100, 300),
(7, 7, 550, 900, 100, 300, 500),
(8, 8, 370, 500, 100, 120, 280),
(9, 9, 800, 600, 200, 100, 300),
(10, 10, 900, 350, 50, 100, 200),
(11, 11, 600, 400, 100, 200, 100),
(12, 12, 500, 600, 60, 140, 400),
(13, 13, 350, 300, 70, 30, 200),
(14, 14, 9000, 200, 100, 50, 50),
(15, 15, 480, 200, 50, 70, 80),
(16, 16, 600, 300, 100, 100, 100),
(17, 17, 450, 180, 80, 20, 80),
(18, 18, 400, 180, 20, 80, 80),
(23, 23, 400, 100, 6, 20, 70),
(30, 30, 600, 200, 40, 10, 150);

-- --------------------------------------------------------

--
-- Структура таблицы `cakes_price`
--

CREATE TABLE `cakes_price` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` int(6) UNSIGNED NOT NULL,
  `quantity` int(6) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cakes_price`
--

INSERT INTO `cakes_price` (`id`, `name`, `price`, `quantity`) VALUES
(1, 'Торт “Киевский”', 400, 35),
(2, 'Торт “Три шоколада”', 560, 24),
(3, 'Торт “Радуга”', 500, 31),
(4, 'Торт “Наполеон”', 320, 40),
(5, 'Торт “Красный бархат”', 420, 35),
(6, 'Торт “Прага”', 370, 19),
(7, 'Торт “Муссовый”', 330, 20),
(8, 'Торт “Медовик”', 430, 37),
(9, 'Торт “Морковка”', 400, 15),
(10, 'Торт “Анна Павлова”', 560, 24),
(11, 'Пирожное “Капкейки“', 40, 120),
(12, 'Пирожное “Профитроли“', 37, 183),
(13, 'Пирожное “Корзинка“', 80, 71),
(14, 'Пирожное “Буше“', 120, 64),
(15, 'Пирожное “Сицилийские трубочки“', 30, 100),
(16, 'Пирожное “Шу“', 75, 93),
(17, 'Молочный шоколад', 60, 200),
(18, 'Горький шоколад', 90, 150),
(23, 'Какао', 120, 500),
(30, 'Клубника', 120, 60);

-- --------------------------------------------------------

--
-- Структура таблицы `cakes_struct`
--

CREATE TABLE `cakes_struct` (
  `id` int(11) NOT NULL,
  `cake_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cakes_struct`
--

INSERT INTO `cakes_struct` (`id`, `cake_id`, `product_id`) VALUES
(1, 4, 1),
(2, 4, 2),
(3, 4, 5),
(4, 4, 6),
(5, 4, 12),
(6, 4, 15),
(7, 4, 16),
(8, 12, 1),
(9, 12, 2),
(10, 12, 5),
(11, 12, 6),
(12, 12, 12),
(13, 12, 15),
(14, 12, 16),
(15, 12, 17),
(16, 12, 7),
(17, 1, 1),
(18, 1, 2),
(19, 1, 5),
(20, 1, 6),
(21, 1, 7),
(22, 1, 12),
(23, 1, 15),
(24, 1, 16),
(25, 1, 17),
(26, 2, 1),
(27, 2, 2),
(28, 2, 5),
(29, 2, 9),
(30, 2, 17),
(31, 2, 19),
(32, 2, 20);

-- --------------------------------------------------------

--
-- Структура таблицы `discount_cards`
--

CREATE TABLE `discount_cards` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `card_number` int(11) NOT NULL,
  `discount` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `discount_cards`
--

INSERT INTO `discount_cards` (`id`, `user_id`, `card_number`, `discount`) VALUES
(1, 6, 1, 0.1),
(2, 7, 2, 0.06),
(3, 8, 3, 0.07),
(4, 9, 4, 0.05),
(5, 10, 5, 0.5),
(6, 11, 6, 0.2);

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `name`) VALUES
(1, 'Мука'),
(2, 'Яйца'),
(3, 'Сыр творожный'),
(4, 'Вода'),
(5, 'Сахар'),
(6, 'Соль'),
(7, 'Какао'),
(8, 'Клубника'),
(9, 'Арахис'),
(10, 'Грецкий орех'),
(11, 'Фундук'),
(12, 'Белок'),
(13, 'Желток'),
(14, 'Желатин'),
(15, 'Разрыхритель'),
(16, 'Ванильный сахар'),
(17, 'Масло сливочное'),
(18, 'Белый шоколад'),
(19, 'Горький шоколад'),
(20, 'Молоко');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `login` varchar(50) NOT NULL,
  `pass` varchar(32) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `pass`, `name`) VALUES
(6, 'login', '83284b166a9a6f3ea7b2392a49f0b5c9', 'name'),
(7, 'log', '83284b166a9a6f3ea7b2392a49f0b5c9', 'name'),
(8, 'email', 'c9d318bd3bd475006805a0c500245cec', 'Михаил'),
(9, 'qqq', '44f2f4f3b3c321010e01b6ad8e42b816', 'qqq'),
(10, 'aaa', '21c50dae30c6acdc71c7b25576b8930a', 'aaa'),
(11, 'login1', '83284b166a9a6f3ea7b2392a49f0b5c9', 'second'),
(12, 'MYLOG', '79a51cfd050a73c0c6f16e03592da521', 'MIHAIL'),
(13, 'tryget', '21c50dae30c6acdc71c7b25576b8930a', 'get'),
(14, 'tryget2', '21c50dae30c6acdc71c7b25576b8930a', 'get2'),
(15, 'ggg', 'be4da288489a194cfd63ad29cee446b3', 'ggg'),
(23, 'logun', '21c50dae30c6acdc71c7b25576b8930a', 'name'),
(24, 'forfor', '21c50dae30c6acdc71c7b25576b8930a', 'spoon'),
(25, 'NEW_USER', '21c50dae30c6acdc71c7b25576b8930a', 'User');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `cakes`
--
ALTER TABLE `cakes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cakes_info`
--
ALTER TABLE `cakes_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cake_price_id` (`cake_price_id`);

--
-- Индексы таблицы `cakes_price`
--
ALTER TABLE `cakes_price`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Индексы таблицы `cakes_struct`
--
ALTER TABLE `cakes_struct`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cake_id` (`cake_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `discount_cards`
--
ALTER TABLE `discount_cards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `cakes`
--
ALTER TABLE `cakes`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `cakes_info`
--
ALTER TABLE `cakes_info`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `cakes_price`
--
ALTER TABLE `cakes_price`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `cakes_struct`
--
ALTER TABLE `cakes_struct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT для таблицы `discount_cards`
--
ALTER TABLE `discount_cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
