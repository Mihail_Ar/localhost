<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Нет, не слипнется</title>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<!-- bootstrap -->
	<link rel="shortcut icon" href="img/ico.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<?php require_once('templates/header.php'); ?>

	<section id="login">
		
		<?php require_once("templates/menu-link.php") ?>
		
		<div class="container forms">
			<div class="row">
				<div class="col-md-5 rectangle">
					<h1 class="form_info">Авторизация</h1>
					<form action="php_scripts/auth.php" method="post">
						<input class="log_input" type="text" name="login" placeholder="Введите логин" required>
						<input class="log_input" type="password" name="pass" placeholder="Введите пароль" required>
						<button class="enter_button log_button" type="submit">ВХОД</button>
					</form>					
				</div>

				<div class="col-md-2"></div>

				<div class="col-md-5 rectangle">
					<h1 class="form_info">Регистрация</h1>
					<form action="php_scripts/regist.php" method="post">
						<input class="log_input" type="text" name="login" placeholder="Введите логин" required>
						<input class="log_input" type="password" name="pass" placeholder="Введите пароль" required>
						<input class="log_input" type="text" name="name" placeholder="Введите имя" required>
						<button type="submit" class="log_button">ЗАРЕГЕСТРИРОВАТЬСЯ</button>
					</form>					
				</div>
			</div>
		</div>
	</section>

	<?php require_once('templates/footer.php'); ?>

</body>
</html>