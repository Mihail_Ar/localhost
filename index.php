<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Нет, не слипнется</title>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<!-- bootstrap -->
	<link rel="shortcut icon" href="img/ico.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<?php require_once('templates/header.php'); ?>

	<section id="main">

		<?php require_once("templates/menu-link.php") ?>
		
		<div class="container rectangle">
			<div class="container" id="info">
				Хотите что-то сладкое, но не можете определиться, что 
				<br>именно?
				<br>Тогда вы попали туда, куда надо! 
				<br>У нас вы найдете большой выбор тортов, десертов, 
				<br>шоколада на любой вкус. В продаже всегда свежая продукция 
				<br>исключительно из натуральных продуктов. 
				<br>Так же вы можете заказать торты с индивидуальным 
				<br>дизайном, которые будут учитывать ваши индивидуальные 
				<br>особенности (непереносимость глютена или лактозы, 
				<br>сахарный диабет и другое).
			</div>
		</div>
		<div class="container title">
			Продукция в наших пекарнях готовится 
			<br>исключительно из натуральных продуктов
		</div>
		<div class="container block">
			<div class="row">
				<div class="col-md-4">
					<img src="img/milk.png" alt="milk">
				</div>
				<div class="col-md-8">
					<h2 class="title">Молоко от местных фермеров</h2>
					<p class="text">
						Для наших изделий мы используем чистое, 
						натуральное молоко без антибиотиков и консервантов, 
						богатое кальцием и минералами от местных фермеров.
					</p>
				</div>
			</div>
		</div>
		<div class="container block">
			<div class="row">
				<div class="col-md-8">
					<h2 class="title">Яйца первой категории</h2>
					<p class="text">
						Яйца для выпечки в наших пекарнях только первой 
						категории от несушек, которые получают экологически 
						чистые корма, содержащие витамины и минеральные 
						добавки. 
					</p>
				</div>
				<div class="col-md-4">
					<img src="img/eggs.png" alt="eggs">
				</div>
			</div>
		</div>
		<div class="container block">
			<div class="row">
				<div class="col-md-4">
					<img src="img/flour.png" alt="flour">
				</div>
				<div class="col-md-8">
					<h2 class="title">Мука высшего сорта</h2>
					<p class="text">
						Мы используем муку приготовленную только из зерна 
						высшего класса, что в свою очередь позволяет ей 
						отличаться превосходными хлебопекарными 
						качествами.
					</p>
				</div>
			</div>
		</div>
		
	</section>

	<?php require_once('templates/footer.php');?>

</body>
</html>